package me.saket.bettermovementmethod;

import ohos.agp.colors.RgbColor;
import ohos.agp.colors.RgbPalette;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ComponentState;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Handles URL clicks on TextViews. Unlike the default implementation, this:
 * <p>
 * <ul>
 * <li>Reliably applies a highlight color on links when they're touched.</li>
 * <li>Let's you handle single and long clicks on URLs</li>
 * <li>Correctly identifies focused URLs (Unlike the default implementation where a click is registered even if it's
 * made outside of the URL's bounds if there is no more text in that direction.)</li>
 * </ul>
 */
public class BetterLinkMovementMethod {

    private static Context context;
    private static BetterLinkMovementMethod singleInstance;
    private static final int LINKIFY_NONE = -2;
    private static OnLinkClickListener onLinkClickListener;
    private static OnLinkLongClickListener onLinkLongClickListener;

    /**
     * OnLinkClickListener
     */
    public interface OnLinkClickListener {
        /**
         * onClick
         *
         * @param textView The TextView on which a click was registered.
         * @param url      The clicked URL.
         * @return True if this click was handled. False to let handle the URL.
         */
        boolean onClick(Text textView, String url);
    }

    /**
     * Set a listener that will get called whenever any link is clicked on the TextView.
     *
     * @param clickListener OnLinkClickListener
     * @return BetterLinkMovementMethod
     */
    public BetterLinkMovementMethod setOnLinkClickListener(OnLinkClickListener clickListener) {
        this.onLinkClickListener = clickListener;
        return this;
    }

    /**
     * Set a listener that will get called whenever any link is clicked on the TextView.
     *
     * @param longClickListener OnLinkLongClickListener
     * @return BetterLinkMovementMethod
     */
    public BetterLinkMovementMethod setOnLinkLongClickListener(OnLinkLongClickListener longClickListener) {
        this.onLinkLongClickListener = longClickListener;
        return this;
    }

    /**
     * 添加监听
     *
     * @param text Text
     */
    public static void addListener(Text text) {
        onLinkClickListener.onClick(text, text.getTag().toString());
        text.setLongClickedListener(new Component.LongClickedListener() {
            @Override
            public void onLongClicked(Component component) {
                onLinkLongClickListener.onLongClick(text, text.getTag().toString());
            }
        });
    }

    public interface OnLinkLongClickListener {
        /**
         * onLongClick
         *
         * @param textView The TextView on which a long-click was registered.
         * @param url      The long-clicked URL.
         * @return True if this long-click was handled. False to let handle the URL (as a short-click).
         */
        boolean onLongClick(Text textView, String url);
    }

    /**
     * Return a new instance of BetterLinkMovementMethod.
     *
     * @return BetterLinkMovementMethod
     */
    public static BetterLinkMovementMethod newInstance() {
        return new BetterLinkMovementMethod();
    }

    /**
     * linkify
     *
     * @param textViews Text控件
     * @return BetterLinkMovementMethod
     */
    public static BetterLinkMovementMethod linkify(Text... textViews) {
        BetterLinkMovementMethod movementMethod = newInstance();
        for (Text text : textViews) {
            linkify(text);
        }
        return movementMethod;
    }

    /**
     * Like but can be used for TextViews with HTML links.
     *
     * @param textViews Text控件
     * @return BetterLinkMovementMethod
     */
    public static BetterLinkMovementMethod linkifyHtml(Text... textViews) {
        return linkify(textViews);
    }

    /**
     * linkify
     *
     * @param componentContainer ComponentContainer
     * @return BetterLinkMovementMethod
     */
    public static BetterLinkMovementMethod linkify(ComponentContainer componentContainer) {
        BetterLinkMovementMethod movementMethod = newInstance();
        rAddLinks(componentContainer);
        return movementMethod;
    }

    /**
     * Like but can be used for TextViews with HTML links.
     *
     * @param viewGroup viewGroup
     * @return The registered {@link BetterLinkMovementMethod} on the TextViews.
     */
    @SuppressWarnings("unused")
    public static BetterLinkMovementMethod linkifyHtml(ComponentContainer viewGroup) {
        return linkify(viewGroup);
    }

    /**
     * linkify
     *
     * @param text Text
     */
    public static void linkify(Text text) {
        StateElement stateElement = new StateElement();
        ShapeElement elementButtonPressed = new ShapeElement();
        elementButtonPressed.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#DCAAD3")));
        elementButtonPressed.setShape(ShapeElement.RECTANGLE);

        ShapeElement empty = new ShapeElement();
        empty.setRgbColor(RgbPalette.TRANSPARENT);
        empty.setShape(ShapeElement.RECTANGLE);

        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_PRESSED}, elementButtonPressed);
        stateElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, empty);
        text.setBackground(stateElement);
    }


    /**
     * Get a static instance of BetterLinkMovementMethod. Do note that registering a click listener on the returned
     * instance is not supported because it will potentially be shared on multiple TextViews.
     *
     * @param context Context
     * @return BetterLinkMovementMethod
     */
    @SuppressWarnings("unused")
    public static BetterLinkMovementMethod getInstance(Context context) {
        if (singleInstance == null) {
            singleInstance = new BetterLinkMovementMethod();
        }
        BetterLinkMovementMethod.context = context;
        return singleInstance;
    }

    /**
     * BetterLinkMovementMethod
     */
    protected BetterLinkMovementMethod() {
    }

    private static void rAddLinks(ComponentContainer viewGroup) {
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            Component child = viewGroup.getComponentAt(i);

            if (child instanceof ComponentContainer) {
                // Recursively find child TextViews.
                rAddLinks(((ComponentContainer) child));
            } else if (child instanceof Text) {
                Text textView = (Text) child;
                linkify(textView);
            }
        }
    }
}
