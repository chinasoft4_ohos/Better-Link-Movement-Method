# Better-Link-Movement-Method

#### 项目介绍
- 项目名称：Better-Link-Movement-Method
- 所属系列：openharmony的第三方组件适配移植
- 功能：实现链接跳转系统应用
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v2.2.0

#### 效果演示

![效果演示](./img/example.gif)
![效果演示](./img/example2.gif)

#### 安装教程

1、在项目根目录下的build.gradle文件中
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
```
2、在entry模块的build.gradle文件中
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:Better-Link-Movement-Method:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

使用该库非常简单，只需查看提供的示例的源代码。

```示例XML
 <Text
              ohos:id="$+id:tv_phone"
              ohos:height="match_content"
              ohos:width="match_content"
              ohos:text="$string:bettermovementmethod_dummy_number"
              ohos:text_color="$color:colorAccent"
              ohos:text_size="45"
              ohos:top_margin="20vp"/>

          <Component
              ohos:height="5"
              ohos:width="260"
              ohos:background_element="#FF4081"/>
```

```java
Intent transferDialogKey = new Intent();
          Operation operation = new Intent.OperationBuilder()
                  .withAction(IntentConstants.ACTION_DIAL)
                  .withUri(Uri.parse("tel:" + "7899859911"))
                  .build();
          transferDialogKey.setOperation(operation);
          startAbility(transferDialogKey);
```




#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原项目组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息
```
Copyright 2018 Saket Narayan.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

```
