package me.saket.bettermovementmethod.sample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    //纯UI组件，无法单独执行单元测试
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("me.saket.bettermovementmethod.sample", actualBundleName);
    }
}