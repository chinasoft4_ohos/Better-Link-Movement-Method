package me.saket.bettermovementmethod.sample.slice;

import me.saket.bettermovementmethod.BetterLinkMovementMethod;
import me.saket.bettermovementmethod.sample.MyToast;
import me.saket.bettermovementmethod.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.PopupDialog;
import ohos.miscservices.pasteboard.PasteData;
import ohos.miscservices.pasteboard.SystemPasteboard;
import ohos.utils.IntentConstants;
import ohos.utils.PacMap;
import ohos.utils.net.Uri;

public class MainAbilitySlice extends AbilitySlice {
    private static final String ADDITON_KEY = "ADDITION_KEY";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        Text tv_Gotham = (Text) findComponentById(ResourceTable.Id_tv_Gotham);
        BetterLinkMovementMethod betterLinkMovementMethod = BetterLinkMovementMethod.getInstance(getContext());
        tv_Gotham.setTag("Long-click:https://en.wikipedia.org/wiki/Gotham_City");
        betterLinkMovementMethod.linkify(tv_Gotham);
        betterLinkMovementMethod.setOnLinkClickListener(urlClickListener)
                .setOnLinkLongClickListener(longClickListener);
        tv_Gotham.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                betterLinkMovementMethod.addListener(tv_Gotham);
            }
        });


        Text tv_tower = (Text) findComponentById(ResourceTable.Id_tv_tower);
        tv_tower.setTag("http://batman.wikia.com/wiki/Wayne_Tower_(Nolan_Films)");
        betterLinkMovementMethod.linkify(tv_tower);
        betterLinkMovementMethod
                .setOnLinkClickListener(urlClickListener)
                .setOnLinkLongClickListener(longClickListener);
        tv_tower.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                betterLinkMovementMethod.addListener(tv_tower);
            }
        });

        Text tv_phone = (Text) findComponentById(ResourceTable.Id_tv_phone);
        tv_phone.setTag("Long-click:tel:7899859911");
        betterLinkMovementMethod.linkify(tv_phone);
        betterLinkMovementMethod
                .setOnLinkClickListener(urlClickListener)
                .setOnLinkLongClickListener(longClickListener);
        tv_phone.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                betterLinkMovementMethod.addListener(tv_phone);
            }
        });

        Text tv_bruce = (Text) findComponentById(ResourceTable.Id_tv_bruce);
        tv_bruce.setTag("Long-click:mailto:bruce@wayne.org");
        betterLinkMovementMethod.linkify(tv_bruce);
        betterLinkMovementMethod
                .setOnLinkClickListener(urlClickListener)
                .setOnLinkLongClickListener(longClickListener);
        tv_bruce.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                betterLinkMovementMethod.addListener(tv_bruce);
            }
        });


        Text tv_location = (Text) findComponentById(ResourceTable.Id_tv_location);
        tv_location.setTag("Long-click:https://goo.gl/maps/KfRBC6KLHA12");
        betterLinkMovementMethod.linkify(tv_location);
        betterLinkMovementMethod
                .setOnLinkClickListener(urlClickListener)
                .setOnLinkLongClickListener(longClickListener);
        tv_location.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                betterLinkMovementMethod.addListener(tv_location);
            }
        });
    }

    private void setCopyTextClick(String address) {
        SystemPasteboard mPasteboard = SystemPasteboard.getSystemPasteboard(MainAbilitySlice.this);
        PasteData pasteData = new PasteData();
        pasteData.addTextRecord(address);
        PacMap pacMap = new PacMap();
        pacMap.putString(ADDITON_KEY, "ADDITION_VALUE_OF_TEXT");
        pasteData.getProperty().setAdditions(pacMap);
        pasteData.getProperty().setTag("USER_TAG");
        pasteData.getProperty().setLocalOnly(true);
        mPasteboard.setPasteData(pasteData);
    }

    private final BetterLinkMovementMethod.OnLinkLongClickListener longClickListener = new BetterLinkMovementMethod.OnLinkLongClickListener() {
        @Override
        public boolean onLongClick(Text textView, String url) {
            MyToast.show(getContext(), url, MyToast.ToastLayout.BOTTOM);
            return true;
        }
    };

    BetterLinkMovementMethod.OnLinkClickListener urlClickListener = new BetterLinkMovementMethod.OnLinkClickListener() {
        @Override
        public boolean onClick(Text view, String url) {
            if (isPhoneNumber(url)) {
                phoneDialog(view);
            } else if (isEmailAddress(url)) {
                mapDialog(view);
            } else if (isMapAddress(url)) {
                locationDialog(view);
            } else {
                MyToast.show(getContext(), url, MyToast.ToastLayout.BOTTOM);
            }

            return true;
        }
    };

    private void phoneDialog(Component view) {
        PopupDialog menuDialog = new PopupDialog(getContext(), view);
        DirectionalLayout menuComponent = (DirectionalLayout) LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_ability_dialog_phone, null, false);
        menuDialog.setCustomComponent(menuComponent);
        Text tv_call = (Text) menuComponent.findComponentById(ResourceTable.Id_tv_call);
        Text tv_send_message = (Text) menuComponent.findComponentById(ResourceTable.Id_tv_send_message);
        Text tv_copy_number = (Text) menuComponent.findComponentById(ResourceTable.Id_tv_copy_number);
        Text tv_save_to_contacts = (Text) menuComponent.findComponentById(ResourceTable.Id_tv_save_to_contacts);
        tv_call.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent transferDialogKey = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withAction(IntentConstants.ACTION_DIAL)
                        .withUri(Uri.parse("tel:" + "7899859911"))
                        .build();
                transferDialogKey.setOperation(operation);
                startAbility(transferDialogKey);
                menuDialog.destroy();
            }
        });

        tv_send_message.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent transferDialogKeys = new Intent();
                Operation operations = new Intent.OperationBuilder()
                        .withAction(IntentConstants.ACTION_SEND_SMS)
                        .withUri(Uri.parse("smsto:" + "7899859911"))
                        .build();
                transferDialogKeys.setOperation(operations);
                startAbility(transferDialogKeys);
                menuDialog.destroy();
            }
        });
        tv_copy_number.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                setCopyTextClick("tel:7899859911");
                menuDialog.destroy();
            }
        });
        tv_save_to_contacts.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent transferDialog = new Intent();
                Operation operation1 = new Intent.OperationBuilder()
                        //.withAction(IntentConstants.ACTION_CHOOSE)
                        .withFlags(Intent.FLAG_NOT_OHOS_COMPONENT)
                        .withBundleName("com.huawei.contacts")
                        .withAbilityName("com.android.contacts.activities.PeopleActivity")
                        .withUri(Uri.parse("tel:" + "7899859911"))
                        .build();
                transferDialog.setOperation(operation1);
                startAbility(transferDialog);
                menuDialog.destroy();
            }
        });
        menuDialog.show();
        menuDialog.setAutoClosable(true);
    }

    private void locationDialog(Component view) {
        PopupDialog menuDialog = new PopupDialog(getContext(), view);
        DirectionalLayout menuComponent = (DirectionalLayout) LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_ability_dialog_navigate, null, false);
        menuDialog.setCustomComponent(menuComponent);
        Text tv_navigate = (Text) menuComponent.findComponentById(ResourceTable.Id_tv_navigate);
        Text tv_copy_address = (Text) menuComponent.findComponentById(ResourceTable.Id_tv_copy_address);
        tv_navigate.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                menuDialog.destroy();
            }
        });

        tv_copy_address.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                setCopyTextClick("https://goo.gl/maps/KfRBC6KLHA12");
                menuDialog.destroy();
            }
        });
        menuDialog.show();
        menuDialog.setAutoClosable(true);
    }

    private void mapDialog(Component view) {
        PopupDialog menuDialog = new PopupDialog(getContext(), view);
        DirectionalLayout menuComponent = (DirectionalLayout) LayoutScatter.getInstance(getContext())
                .parse(ResourceTable.Layout_ability_dialog_email, null, false);
        menuDialog.setCustomComponent(menuComponent);
        Text tv_send_email = (Text) menuComponent.findComponentById(ResourceTable.Id_tv_send_email);
        Text tv_capy_email_address = (Text) menuComponent.findComponentById(ResourceTable.Id_tv_capy_email_address);
        tv_send_email.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                menuDialog.destroy();
            }
        });

        tv_capy_email_address.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                setCopyTextClick("mailto:bruce@wayne.org");
                menuDialog.destroy();
            }
        });

        menuDialog.show();
        menuDialog.setAutoClosable(true);
    }

    private boolean isPhoneNumber(String url) {
        return url.contains("tel:");
    }

    private boolean isEmailAddress(String url) {
        return url.contains("@");
    }

    private boolean isMapAddress(String url) {
        return url.contains("goo.gl/maps");
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
